# Frank Experiment 1: Word Segmentation, Sentence Length

Code used to reproduce Frank et al., 2010. [Modeling human performance in
statistical word
segmentation](https://dspace.mit.edu/openaccess-disseminate/1721.1/102506).

## Running the experiment

### Setup

#### Unzipping stimuli

Unzip `langs.zip`, obtainable from the McGuinn lab server
(`Psychology/Hartshornelab/Resources/Stimuli/frankexp1_word_segmentation_length`)
in the `static/stims` directory. Make sure there is no nested `langs/` folder,
i.e. `/static/stims/langs/lang_001`. Each language folder (`lang_001`) and the
extra .js files (`test_files_index.js`, `train_lengths.js`) should be at the
root level of the `static/stims` directory: `static/stims/lang_001/`,
`static/stims/lang_003/`, `static/stims/test_files_index,js`,
`static/stims/train_lengths.js`, etc.

Then, run `psiturk` from the root directory.

#### Making stimuli

Making stimuli (not just unzipping) requires `sox`, `lame`, and an os-specific
version of the `mbrola` binary in `static/stims/` (included by default is the
Mac OS X binary).

To make all 50 languages, run `make` in the directory. Takes about an hour. To
make just one language for testing, run `make one`. `make clean` removes the
language files (but not the zip).

#### psiTurk parameters

You must set num_conds = 8 and num_counters = 50 in psiTurk's `config.txt` task
parameters, or the experiment won't work (will always give the same language).
See `config.txt.example` for an example psiTurk config file.

#### Time

I measure conservatively 20 seconds for 3 test question. Therefore the test
phase probably lasts 240 seconds = 4 minutes. Say 5 minutes for safety.

I think we can safely say the experiment last around 20 minutes. Almost all
recordings, regardless of length condition are less than 15 minutes (The
longest is 12:50).  But there is probably reading and demographics in between.
Payment should be in accordance with that estimate.

#### Note about test pairs

Test pairs encoded into metadata have the form

`[[w0, w1], file_name, which_correct, is_catch]`

### Parsing results

Given data downloaded through psiTurk, run `parse_trialdata.py` first, then
`clean_trialdata.R`, which creates three cleaned .csv files:
`tdata_all_clean.csv`, `tdata_missone_clean.csv`, and
`tdata_missany_clean.csv`. Explanations of these files are located in the
RMarkdown.

## Writing + analysis

Both the writeup and analyses are (thanks to RMarkdown) located in
`writeup/frankexp1_word_segmentation_length.Rmd`. Additional exploratory
analysis is located in `analysis.Rmd`.

Knitting the RMarkdown requires the
[`kemacdonald/cogsci2016`](https://github.com/kemacdonald/cogsci2016) package
installed via `devtools`, the other packages loaded in the `Rmd` file, and a
TeX distribution. Written in RMarkdown; RStudio knits to tex and then pdf.

You can swap one of the `tdata` files with another (to run with a different
version of the trialdata), and everything will *mostly* work, except for
assertions I make at certain points about p values - just remove those and you
should be good to go.
