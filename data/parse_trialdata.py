"""
Parse trialdata
"""

DATAFILE = '1.0/1.0.csv'

import json
import pandas as pd

if __name__ == '__main__':
    df = pd.read_csv(DATAFILE)
    tuple_keys = ['index'] + list(df.columns)
    # Loop through dataframe, grab dicts of relevant.
    dicts = []
    for row in df.itertuples():
        # Keep uniqueid, condition, counterbalance
        row_dict = dict(zip(tuple_keys, row))

        # Parse inner JSON datastring
        datastring = json.loads(row_dict['datastring'])
        # Delte from dict now
        del row_dict['datastring']

        # Now expand on trialdata. This is the list of trials
        inner_data = datastring['data']

        inner_data_parsed = []
        for data in inner_data:
            # Get trialdata, then delete it form original
            trialdata = data['trialdata'].copy()
            del data['trialdata']
            if 'responses' in trialdata:
                responses = json.loads(trialdata['responses'])
                del trialdata['responses']
            else:
                responses = {}
            # Merge EVERYTHING
            all_merged = dict(trialdata.items() + data.items() +
                              responses.items() + row_dict.items())
            inner_data_parsed.append(all_merged)

        dicts.extend(inner_data_parsed)

    df_complete = pd.DataFrame(dicts)

    df_complete.to_csv('trialdata_parsed.csv')
