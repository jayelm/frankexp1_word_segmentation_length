/* remember to make sure that all the necessary audio files are in the '/static/audio' folder! */

// var LANGS_ROOT = '/static/stims/';
var LANGS_ROOT = 'https://d2dg4e62b1gc8m.cloudfront.net/turk/langaq_17/frankexp1_word_segmentation_length/langs/';

var LENGTHS = [1, 2, 3, 4, 6, 8, 12, 24];

var sample_language = function() {
    n_lang = langs.length;

    // Index into actual length given lang_i. Notice that lang_i doesn't
    // correspond directly
    var lang = langs[lang_i];
    var lang_folder = lang[0];
    var lang_test = lang[1];

    // NOW: SHUFFLE LANG_TEST BASED ON WORDS
    lang_test = shuffle_lang_test(lang_test);
    // Verify uniqueness
    verify_no_adjacent_duplicates(lang_test);

    var file_prefix = LANGS_ROOT + lang_folder + '/';

    // Get actual length from length_condition_i
    var length_condition = LENGTHS[length_condition_i];

    var train_file = file_prefix + 'train_' + length_condition + '.mp3';

    // Add 5 additional seconds to training length
    var train_length = train_lengths[length_condition.toString()] + 5000;

    var questions = [];

    for (var i = 0; i < lang_test.length; i++) {
        lt = lang_test[i];
        // LTs have the format
        // [words, file, correct, is_catch]
        questions.push({
            // Test info: filepath, real word, words 1 and 2, and word length
            filepath: file_prefix + lt[1],
            filepath_local: lt[1],
            real_word: lt[2],
            w0: lt[0][0],
            w1: lt[0][1],
            word_length: lt[0][0].length / 2,
            catch: lt[3],
            // Condition info, as well as psiturk counterbalancing indices
            // (just for sanity check)
            length_condition: length_condition,
            length_condition_i: length_condition_i,
            lang: lang_folder,
            lang_i: lang_i,

            // Test phase, in case this isn't obvious
            phase: 'test'
        });
    }

    return {
        training: train_file,
        train_length: train_length,
        questions: questions
    };
};

// FUNCTIONS FOR SHUFFLING QUESTIONS. Inefficient JS translations of python codes
var shuffle_lang_test = function(combs) {
    var n = combs.length;
    var init_combs = combs.slice(); // i.e. copy
    var select = [];
    var comb_samp = null;
    for (var i = 0; i < n; i++) {
        var remain = not_same(comb_samp, combs);
        if (remain.length === 0) {
            // If a duplicate and now way to resample, restart
            console.log("Restarting");
            return shuffle_lang_test(init_combs, n);
        }

        // Sample from next possible choices
        comb_samp = remain[Math.floor(Math.random() * remain.length)];

        // Remove this value so that it cannot be sampled again
        var to_del_index = combs.indexOf(comb_samp);
        combs.splice(to_del_index, 1);

        select.push(comb_samp);
    }

    return select;
};


var not_same = function(samp, x) {
    if (samp === null) {
        // Don't do any filtering
        return x;
    }

    // Get words from samp
    samp = samp[0];

    var i;

    // Filter f1, only keep those where 0th element is different
    var x_f1 = [];
    for (i = 0; i < x.length; i++) {
        t = x[i];
        words = t[0]; // Get words
        if (words[0] !== samp[0] && words[0] !== samp[1]) {
            x_f1.push(t);
        }
    }

    // Filter f2, only keep those where 0th element is different
    var x_f2 = [];
    for (i = 0; i < x_f1.length; i++) {
        t = x_f1[i];
        words = t[0]; // Get words
        if (words[1] !== samp[1] && words[1] !== samp[0]) {
            x_f2.push(t);
        }
    }

    return x_f2;
};


var verify_no_adjacent_duplicates = function(combs) {
    for (var i = 0; i < combs.length - 1; i++) {
        prev = combs[i][0]; // 0-index to get words
        next = combs[i + 1][0];
        if (prev[0] === next[0] || prev[1] === next[1] || prev[0] === next[1] || prev[1] === next[0]) {
            console.log([prev, next]);
            throw 'non-adjacent duplicates';
        }
    }
};
