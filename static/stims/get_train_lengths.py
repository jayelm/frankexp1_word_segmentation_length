"""
Write lengths of file to a javascript file.
NOTE THAT ALL TRAINING LENGTHS ARE THE SAME given the sampling procedure. So
this file just does a quick assertion that they all generally looks the same
then writes the training lengths out
"""

import glob
import subprocess
import json

if __name__ == '__main__':
    i = 0
    durs = {}
    for lang in glob.glob('lang_*'):
        # Probably just need to check 25 before you're sure
        if i > 25:
            break
        for train in glob.glob('{}/train_*.mp3'.format(lang)):
            soxi_cmd = 'soxi {}'.format(train)
            out = subprocess.check_output(soxi_cmd, shell=True)
            out_lines = map(lambda x: x.strip(), out.split('\n'))

            # Get dur
            sample_rate = int(filter(
                lambda x: x.startswith('Sample Rate'), out_lines
            )[0].split(':')[1].strip())
            assert sample_rate == 16000

            dur_line = filter(lambda x: x.startswith('Duration'), out_lines)[0]
            # Divide by samples per second, slightly greater macroscopy will
            # assert they're all equal
            dur = int(dur_line.split('samples')[0].split('=')[-1].strip()) / 16000

            train_name = int(
                train.split('/')[1].split('_')[1].split('.mp3')[0]
            )
            if train_name in durs:
                assert durs[train_name] == dur
            else:
                durs[train_name] = dur

        i += 1

    # Convert back to ms
    durs = {k: v * 1000 for k, v in durs.iteritems()}

    with open('train_lengths.js', 'wb') as fout:
        fout.write('var train_lengths = {};'.format(json.dumps(durs)))
