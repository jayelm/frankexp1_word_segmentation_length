#!/bin/sh

zip -r langs.zip lang_* test_files_index.js train_lengths.js

echo "Unzip langs.zip in static/stims. This replaces running make in that directory" > langs_readme.txt
