"""
# WORDS ====
Generate words for frank exp 1

Given syllables, shuffle them and concat into "six words, two with two
syllables, two with three syllables, and two with four syllables"

# TRAIN ====
Generate sentences for frank exp 1, with given length conditions

Given words, make sentences independently for each length condition.
Sentences are formed by randomly sampling from words

# TEST ====
Generate 30 target/distractor pairs, where

- targets are words from the lexicon
- distractors are part-word foils as made in Saffran et al. (1996)

Assumption here is that, as in Saffran, we generate 6 part-word foils, then
exhaustively pair 6 real words with 6 part-word foils (= 36 pairs). Finally,
we shuffle, cut off the last 6 elements (to have 30 instead of 36), then
ensure there are no adjacent repeats of nonwords or partwords. If there is,
then we just reshuffle (should be fairly straightforward for that to
terminate).
"""

# LIBRARIES ====

import subprocess
import random
import numpy as np
from collections import Counter
import itertools
import os
import json

# CONSTANTS ====

# Tools
MBROLA_CMD = './mbrola'
SOX_CMD = 'sox'
LAME_CMD = 'lame'
VOICE = 'us2/us2'

# Syllable vars
SYLLABLES = [
    'ba', 'bi', 'da', 'du', 'ti', 'tu', 'ka', 'ki', 'la', 'lu', 'gi', 'gu',
    'pa', 'pi', 'va', 'vu', 'zi', 'zu'
]

# Catch trials. Note that I try to enforce that these are composed of two
# characters, so I use numbers to denote the catches
# These are selected by hand. They include some new consonants, but
# importantly, all of the vowels do not appear in any language (hence the
# numbers)
CATCHES = {
    2: 'n1m1',
    3: 'h0s1y3',
    4: 't4m0n0l3'
}

# As in Frank, Consonant is 25ms followed by 225ms vowel. Pitch is 100Hz for
# 100% of the utterance
PHO_FORMAT = """
{consonant} 25 100 {hz}
{vowel} 225 100 {hz}
""".strip()

# 500ms of silence
SILENCE = "_ 500"

# Adding 1ms of space seems to prevent segmentation faults
PHO_WRAPPER = "_ 1\n{}\n_ 1"

# Standard IPA to us2/us2 IPA. See us2.txt for reference.
PHONEME_MAP = {
    # Vowels
    'i': 'i',
    'a': 'A',
    'u': 'u',
    # Consonants
    'b': 'b',
    'd': 'd',
    't': 't_h',
    'k': 'k_h',
    'l': 'l',
    'g': 'g',
    'p': 'p_h',
    'v': 'v',
    'z': 'z',
    # Phonemes that aren't in the training data - used as catch trials
    '0': '@U',  # o as in "over"
    '1': 'I',  # i as in "bit"
    '3': '@',  # schwa
    '4': 'EI',  # schwa
    # And consonants
    'n': 'n',
    'm': 'm',
    'l': 'l',
    'y': 'j',
    's': 's',
    'h': 'h'
}

# Word vars
WORD_LENGTHS = [2, 2, 3, 3, 4, 4]

# Train vars
LENGTH_CONDITIONS = [1, 2, 3, 4, 6, 8, 12, 24]
N_TRAIN_WORDS = 600

# Test vars
N_TEST_PAIRS = 30


# FUNCTIONS ====
def weighted_sample(supply):
    """
    Conduct a weighted sample with probabilities proportional to the number of
    tokens left in the supply. This means that words with extra uses left have
    a higher chance of being selected.
    """
    words, weights = zip(*supply.items())
    words_left = float(sum(weights))
    if words_left == 0:
        raise ValueError("Cannot sample, no supply")
    probs = [w / words_left for w in weights]
    return np.random.choice(words, p=probs)


def chunks(lst, n):
    """Chunk a list into sublists of length n"""
    cs = []
    for i in xrange(0, len(lst), n):
        cs.append(lst[i:i + n])
    return cs


def w2s(word):
    """Given a word, convert into syllables (just every 2 chars)"""
    return chunks(word, 2)


def make_pho_file(syls, pho_file=None, directory='.'):
    """
    Generate the pho file with the given syls and return its name
    """
    phos = []
    for syl in syls:
        if syl is None:
            # Silence
            pho = SILENCE
        else:
            const, vowel = syl
            pho = PHO_FORMAT.format(
                consonant=PHONEME_MAP[const],
                vowel=PHONEME_MAP[vowel],
                hz=100
            )
        phos.append(pho)

    if pho_file is None:
        pho_file = '{}.pho'.format(''.join(syls))

    pho_file = directory + '/' + pho_file

    phos_wrapped = PHO_WRAPPER.format('\n'.join(phos))

    with open(pho_file, 'wb') as fout:
        fout.write(phos_wrapped)

    return pho_file


def mbrola(pho_file, to_mp3=True):
    """
    Run mbrola on the pho_file, save as the equivalent .wav file.

    to_mp3 flag, if set, will instead output an mp3 file, by first outputting
    the wav file then encoding it with LAME.
    """
    wav_file = pho_file.replace('.pho', '.wav')
    mbrola_cmd = '{} {} {} {}'.format(
        MBROLA_CMD, VOICE, pho_file, wav_file
    )
    subprocess.check_call(mbrola_cmd, shell=True)

    # Then, convert
    if to_mp3:
        lame_cmd = '{lame} {wav} {mp3}'.format(
            lame=LAME_CMD,
            wav=wav_file,
            mp3=wav_file.replace('.wav', '.mp3')
        )
        subprocess.check_call(lame_cmd, shell=True)
        # Lastly, remove the wav
        os.remove(wav_file)


def gen_supply_exc(supply, word):
    """
    Given the supply and the sampled word, return a new supply which is the
    supply without the given word. if this ends up returning an empty
    dictionary, then we can't do any more sampling!
    """
    new_supply = supply.copy()
    del new_supply[word]
    return new_supply


def gen_sentence(supply, sentence_length):
    """
    Generate a sentence with the given supply of words under the restriction
    that a single word cannot appear twice in a row. If this is not possible,
    return None
    """
    supply = supply.copy()
    sentence = []
    for _ in xrange(sentence_length):
        word_samp = weighted_sample(supply)
        if sentence and sentence[-1] == word_samp:
            # Resample by deleting the word
            supply_exc = gen_supply_exc(supply, word_samp)

            if sum(supply_exc.values()) == 0:
                # Then I couldn't have sampled the above word. Probably (by
                # some justification) I will never be able to sample this
                # sentence, since eventually I have to have a repeat??
                return None

            # Otherwise sample while excluding this word
            word_samp = weighted_sample(supply_exc)

        sentence.append(word_samp)
        # Decrement supply
        supply[word_samp] -= 1

    # Outside of this function, be sure to update supply
    return sentence


def remove_last(supply, last):
    """
    Given the supply and a sentence, remove the words of the sentence from
    the supply
    """
    for word in last:
        supply[word] -= 1


def gen_sentences(n_train_words, supply, sentence_length):
    """
    Sample sentences of `sentence_length` with a total of `n_train_words`
    randomly, with two constraints:

        1. We sample from the given number of words in the supply, and
        2. In a sentence (but NOT between sentences), there are no adjacent
           repetitions of words

    Supply is a dictionary mapping words to the number of counts left. At each
    step a weighted random sample of the supply is selected.

    Note we assume (and check later) that sentence_length divides n_train_words
    evenly, and that the total supply of words is exactly equal to
    n_train_words.

    This does a naive restart if we start sampling but then end up with too
    many leftover tokens of one type, which would require that we have adjacent
    repetitions. Under the conditions of Frank et al., it never takes more than
    2 or 3 restarts to generate a compliant sample, although theoretically this
    could run for a very long time.
    """

    sentences = []

    # Save for possible recursive call
    init_supply = supply.copy()
    init_n_train_words = n_train_words

    while n_train_words > 0:
        new_sentence = gen_sentence(supply, sentence_length)
        if new_sentence is None:
            # Then can't sample. Could be easy enough to add backtracking,
            # but as it turns out, this runs fairly reliably if you just
            # restart the sampling procedure
            print "Restarting condition {}".format(sentence_length)
            return gen_sentences(init_n_train_words,
                                 init_supply,
                                 sentence_length)
        else:
            # Add the sentence and decrease the supply
            sentences.append(new_sentence)
            n_train_words -= sentence_length
            remove_last(supply, new_sentence)

    assert n_train_words == 0
    assert sum(supply.values()) == 0

    return sentences


def test_valid(sentences, n_train_words, n_words, length_condition):
    """
    Verify training data follows the rules, i.e.
    1) No adjacent repeats in sentences
    2) all sentences of specified length
    """
    n_per_word = float(n_train_words) / n_words
    # Should divide evenly
    assert float(int(n_per_word)) == n_per_word, "Doesn't divide evenly"
    n_per_word = int(n_per_word)

    word_counter = Counter()
    for s in sentences:
        assert len(s) == length_condition, "Wrong sentence length".format(s)
        for w0, w1 in itertools.izip_longest(s, s[1:]):
            if w1 and w1 == w0:
                raise AssertionError(
                    "Sentence contains duplicate words: {} {}".format(w0, w1)
                )
            word_counter[w0] += 1

    # Assert totals
    assert sum(word_counter.values()) == n_train_words, "Not enough words"
    # Assert n_per_word
    assert all(n == n_per_word for n in word_counter.values()), "wordcount off"


def gen_train_file(sentences, length_condition, directory):
    """
    Generate training data given list of sentences. This first joins a list of
    sentences into a single .pho file containing a list of mbrola commands -
    specifically, sentences are represented as a continuous stream of phonemes
    (nothing distinguishing words) and sentences are separated by 500ms of
    silence.
    """
    if not sentences:
        raise ValueError("Empty sentences")
    # Flatten sentences (list of lists)
    s_flat = []
    for sentence in sentences:
        for word in sentence:
            # Split this back up into syllables
            s_flat.extend(w2s(word))
        # At the end of each sentence, append None
        s_flat.append(None)
    # Shave off last None
    s_flat = s_flat[:-1]

    train_file = 'train_{}.pho'.format(length_condition)

    return make_pho_file(s_flat, train_file, directory)


def gen_partwords(words, test_per_word, word_lens):
    """
    READ CLOSELY HERE, FROM FRANK:

    Test materials consisted of 30 target-distractor pairs. Each pair consisted
    of a word from the lexicon paired with a "partword" distractor with the
    same number of syllables. Partword distractors were created as in (Saffran,
    Newport, & Aslin, 1996): they were sequences of syllables of the same
    lengths as words, composed of the end of one word and the beginning of
    another (e.g. in a language with words badutu and kagi, a part word might
    be dutuka, which combines the last two syllables of the first word with the
    first syllable of the second). In all conditions except the length 1
    condition, partword sequences appeared in the corpus (although with lower
    frequency than true words). No adjacent test pairs contained the same words
    or part-word distractors.

    This is DIFFERENT from Saffran et al. Frank explicitly discusses grabbing
    WORD BOUNDARIES which is not exactly what Saffran does. We do that here.
    Idea is to grab all possible word boundaries, (the product of the words
    minus adjacent word repeats), shuffle them, then make partwords of various
    lengths out of them.

    FIXME: Special case in Frank where a 4-syllable partword could entirely
    subsume two 2-syllable true words. Since the method seems to imply that
    part-words are strict subsets of true words, what's a sensible way of
    fixing this?
    """
    # Words for assertion later
    words_strs = map(''.join, words)

    # These are all possible word boundaries
    boundaries = list(itertools.product(words, words))
    # Filter boundaries which are the exact same words cross-boundary
    boundaries = filter(lambda t: t[0] != t[1], boundaries)

    pws_dict = {}

    # XXX: Fixing partword subsumption...basically, let's sample 5 4-length
    # partwords...from boundaries WITHOUT two-syllable words. Then we
    # concatenate them back in and do the chunking as normal
    bounds_2 = filter(
        lambda x: len(x[0]) == 2 or len(x[1]) == 2, boundaries
    )
    bounds_no_2 = filter(
        lambda x: not (len(x[0]) == 2 or len(x[1]) == 2), boundaries
    )
    assert bounds_2
    assert bounds_no_2

    random.shuffle(bounds_no_2)
    bounds_no_2_select = bounds_no_2[:test_per_word]
    bounds_no_2_noselect = bounds_no_2[test_per_word:]

    assert bounds_no_2_select
    assert bounds_no_2_noselect

    pws_4 = []
    for w0, w1 in bounds_no_2_select:
        how_much_w0 = how_much_w1 = len(w0) + len(w1)

        while how_much_w1 > len(w1) or how_much_w0 > len(w0):
            # not 4 because there might not be 4 (see below)
            how_much_w0 = random.randrange(1, 3)
            # But word_len is 4 in this case
            how_much_w1 = 4 - how_much_w0

        # Piece together!
        new = w0[-how_much_w0:][:]
        new.extend(w1[:how_much_w1][:])

        assert len(new) == 4, 'Sampled wrong-length partword in 4'

        # Since we sample w/o repeats...the partword should never be equal
        # to a given word, but just to make sure...
        assert ''.join(new) not in words_strs

        metadata = {
            'w0': ''.join(w0),
            'w1': ''.join(w1),
            'how_much_w0': how_much_w0,
            'how_much_w1': how_much_w1
        }

        pws_4.append((new, metadata))

    pws_dict[4] = pws_4

    # Now recombine boundaries
    boundaries = bounds_no_2_noselect + bounds_2

    # With the remaining boundaries, shuffle them
    random.shuffle(boundaries)
    # Now that they're shuffled, we'll take `test_per_word` per word len
    pw_bounds = chunks(boundaries, test_per_word)

    assert len(word_lens) == 3

    for word_len, bounds in zip(word_lens, pw_bounds):
        if word_len == 4:
            continue  # We did this earlier
        # Here we grab the boundary. Note this depends on the word length.
        # Basically, generate a random number that measures how much of the
        # first word to get. Note that in our case, since the max partword
        # length is 4 and the min partword length is 2, we may sometimes
        # subsume the entire first word, but we will never subsume more than
        # the first word.
        pws = []
        for w0, w1 in bounds:
            how_much_w0 = how_much_w1 = len(w0) + len(w1)

            # SPECIAL CASE: second word is a 2 syllable and you've sampled only
            # 1 from how_much_w0. This makes how_much_w1 3, which
            # over-samples. As a brute force hack, just resample until
            # how_much_w1 is the right length

            while how_much_w1 > len(w1) or how_much_w0 > len(w0):
                # Don't grab > 2 if word_len == 4
                how_much_w0 = random.randrange(1, min(3, word_len))
                how_much_w1 = word_len - how_much_w0

            # Piece together!
            new = w0[-how_much_w0:][:]
            new.extend(w1[:how_much_w1][:])

            assert len(new) == word_len, 'Sampled wrong-length partword'

            # Since we sample w/o repeats...the partword should never be equal
            # to a given word, but just to make sure...
            assert ''.join(new) not in words_strs

            metadata = {
                'w0': ''.join(w0),
                'w1': ''.join(w1),
                'how_much_w0': how_much_w0,
                'how_much_w1': how_much_w1
            }

            pws.append((new, metadata))

        pws_dict[word_len] = pws

    return pws_dict


def adjacent_duplicates(combs):
    for w0, w1 in zip(combs, combs[1:]):
        # This works regardless of whether or not test pairs have been shuffled
        # in presentation order
        if (w0[0] == w1[0] or w0[1] == w1[1] or
                w0[0] == w1[1] or w0[1] == w1[0]):
            return True
    return False


def not_same(samp, x):
    """
    Remove elements of the list which have same element 1 or element 2. Allows
    for sampling from a list of test pairs without adjacent repeats.
    """
    if samp is None:
        # Don't do any filtering
        return x
    x_f1 = filter(lambda t: t[0] != samp[0] and t[0] != samp[1], x)
    x_f2 = filter(lambda t: t[1] != samp[1] and t[1] != samp[0], x_f1)
    return x_f2


def sample_tests(tests, n):
    """
    Sample n pairs from the list of test pairs, ensuring no adjacent
    duplicates. Like gen_train_file, this does a brute force re-sample if we
    end up with adjacent repetitions at the end of smapling.

    When n = len(tests) this is a shuffle with the above restrictions.
    """
    init_tests = tests[:]
    select = []
    test_samp = None
    for _ in xrange(n):
        remain = not_same(test_samp, tests)
        if len(remain) == 0:
            # If a duplicate and no way to resample, restart
            print "Restarting"
            return sample_tests(init_tests, n)

        # Sample from next possible choices
        test_samp = random.choice(remain)

        # Remove this value so that it cannot be sampled again
        del tests[tests.index(test_samp)]

        select.append(test_samp)

    return select


def shuffle_order(test_pairs):
    """
    Randomly switch the order of the test pairs
    """
    new_test_pairs = []
    for a, b in test_pairs:
        if random.randrange(2):
            new_test_pairs.append((a, b))
        else:
            new_test_pairs.append((b, a))
    return new_test_pairs


def gen_test_files(test_pairs, directory):
    """
    Given the test files, make the .pho file and run mbrola.
    """
    for a, b in test_pairs:
        a_str = ''.join(a)
        b_str = ''.join(b)
        syls = []
        syls.extend(a)
        syls.append(None)
        syls.extend(b)
        test_file = 'test_{}_vs_{}.pho'.format(a_str, b_str)
        print "Making test pair {} vs {}".format(a_str, b_str)
        test_file = make_pho_file(syls, test_file, directory)
        mbrola(test_file)


if __name__ == '__main__':
    import sys
    # Make directory
    try:
        argv_n = sys.argv[1]
    except IndexError as e:
        print "Specify number of language (e.g. 1)"
        raise e
    if len(argv_n) == 1:
        argv_n = '00' + argv_n
    elif len(argv_n) == 2:
        argv_n = '0' + argv_n
    file_loc = 'lang_{}'.format(argv_n)
    if not os.path.exists(file_loc):
        os.makedirs(file_loc)

    # WORDS ====
    syllables = random.sample(SYLLABLES, len(SYLLABLES))

    # Word lengths should use all syllables
    assert sum(WORD_LENGTHS) == len(syllables)

    i = 0
    words = []
    for word_len in WORD_LENGTHS:
        words.append(syllables[i:i + word_len])
        i += word_len

    for word_syls in words:
        print "Creating word {}".format(''.join(word_syls))
        pho_file = 'word_{}_{}.wav'.format(word_len, ''.join(word_syls))
        pho_file = make_pho_file(word_syls, pho_file, file_loc)
        mbrola(pho_file)

    # TRAIN ====
    n_words = len(words)
    n_per_word = N_TRAIN_WORDS / n_words
    # Supply of words to sample from: equal parts of all words
    init_supply = {''.join(word): n_per_word for word in words}

    all_train = []

    for sentence_length in LENGTH_CONDITIONS:
        print "Generating sentences of length {}".format(sentence_length)
        # This samples with the restrictions of the supply
        sentences = gen_sentences(N_TRAIN_WORDS, init_supply.copy(),
                                  sentence_length)

        # Make sure that the generated sentence is valid
        test_valid(sentences, N_TRAIN_WORDS, n_words, sentence_length)

        print "Making training data for length {}".format(sentence_length)
        # Write to file
        train_file = gen_train_file(sentences, sentence_length, file_loc)
        mbrola(train_file)

        all_train.append((sentence_length, sentences))

    # TEST ====
    # Generate pairs by pairing each true word with all the part words. So if
    # there are 6 true words, and 5 partwords per word length, each true word
    # appears 5 times with all 5 partwords associated with the given word
    # length. In our experiment, this makes 6 * 5 = 30 pairs, but notice there
    # are 15 (5 x 3 partwords) and 6 true words. So each true word occurs 5x,
    # but each partword occurs 2x. Is that a problem?

    # XXX: UPDATE: each true word also appears with the catch trial associated
    # with the word lenght. So each true word appears once more - 30 + 6 = 36
    # trials. Furthermore, each catch trial appears twice (once with the two
    # true words of the specified word length).
    test_per_word = N_TEST_PAIRS / n_words
    uniq_word_lens = list(set(WORD_LENGTHS))
    # Verbose contains extra metadata information
    pws_dict_verbose = gen_partwords(words, test_per_word, uniq_word_lens)
    # pws_dict maps unique word lengths (e.g. 2, 3, 4) to the partwords of that
    # word length
    pws_dict = {}
    for k, v in pws_dict_verbose.iteritems():
        pws_dict[k] = map(lambda x: x[0], v)

    test_pairs = []
    for word_len in uniq_word_lens:
        # Get the partwords associated with the given word length
        these_pws = pws_dict[word_len]
        # XXX: Update: add associated catch trial too. These are strings, so
        # break apart.
        these_pws.append(w2s(CATCHES[word_len]))
        these_words = filter(lambda x: len(x) == word_len, words)
        # Exhaustive combinations of the true words in the word length, and the
        # part words
        these_pairs = list(itertools.product(these_words, these_pws))
        test_pairs.extend(these_pairs)

    # FIXME: Catches +6 hardcoded. Figure out a way to change this.
    assert len(test_pairs) == N_TEST_PAIRS + 6

    # Randomly shuffle test paris such that there are no adjacent repetitions
    # of true/part words. Note this is repeated in the javascript code for each
    # participant
    test_pairs = sample_tests(test_pairs, len(test_pairs))
    assert not adjacent_duplicates(test_pairs)

    # Finally, shuffle the order of real word/part word presentation of
    # test_pairs
    test_pairs = shuffle_order(test_pairs)

    gen_test_files(test_pairs, file_loc)

    # METADATA ====
    # This is pretty messy, just trying to get everything into a metadata file
    # so that it can be conveniently accessed later for analysis.
    partwords_flat = [item for sublst in pws_dict.values() for item in sublst]
    partwords_flat = map(''.join, partwords_flat)
    # Filter out catches
    partwords_flat = filter(
        lambda x: x not in CATCHES.values(), partwords_flat
    )
    pws_dict_str = {}
    for k, v in pws_dict_verbose.iteritems():
        pws_dict_str[k] = map(lambda x: [''.join(x[0]), x[1]], v)

    words_str = map(''.join, words)

    test_pairs_str = []
    for tp in test_pairs:
        # Regenerate filename.
        assert len(tp) == 2
        tp_0_str = ''.join(tp[0])
        tp_1_str = ''.join(tp[1])
        tp_fname = 'test_{}_vs_{}.mp3'.format(tp_0_str, tp_1_str)
        # This is 0 or 1 depending on which number is the true word
        which_correct = int(tp_1_str in words_str)
        if not which_correct:
            assert tp_0_str in words_str
        # This is 0 or 1 depending on which
        is_catch = (
            '0' in tp_0_str or '1' in tp_0_str or
            '3' in tp_0_str or '4' in tp_0_str or
            '0' in tp_1_str or '1' in tp_1_str or
            '3' in tp_1_str or '4' in tp_1_str
        )

        test_pairs_str.append(
            [map(''.join, tp), tp_fname, which_correct, is_catch]
        )

    # Create dict. Since test_pairs_str has filenames, we can access all
    # necessary stim files from the metadata. Just prepend /stims/ to the
    # urls in the javascript
    metadata = {
        'words': words_str,
        'partwords_dict': pws_dict_str,
        'partwords': partwords_flat,
        'test_pairs': test_pairs_str,
        'train': {},
        'catches': CATCHES
    }
    # Add train stimuli
    for sentence_length, sentences in all_train:
        metadata['train'][sentence_length] = sentences

    with open(file_loc + '/' + file_loc + '_metadata.json', 'wb') as fout:
        json.dump(metadata, fout)

    # Short file containing test_files. Will be concatenated later - needed for
    # sampling languages and knowing where the test files are
    with open(file_loc + '/' + file_loc + '_test_files.json', 'wb') as fout:
        json.dump(test_pairs_str, fout)
