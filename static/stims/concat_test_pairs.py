"""
Concatenates test_files
"""

import glob
import os

if __name__ == '__main__':
    tfs = []
    for lang in glob.glob('lang_*'):
        tf_file = '{lang}/{lang}_test_files.json'.format(lang=lang)
        if os.path.exists(tf_file):
            with open(tf_file, 'rb') as fin:
                tf_content = fin.read()
            tfs.append('["{}", {}]'.format(lang, tf_content))

    total = ','.join(tfs)
    total_str = 'var langs = [{}];'.format(total)
    comment = '// [[w0, w1], file_name, which_correct, is_catch]\n'
    total_str = comment + total_str

    with open('test_files_index.js', 'wb') as fout:
        fout.write(total_str)
