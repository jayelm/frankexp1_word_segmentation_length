## Make Base Syllables ##
- For the experiment, we need to create the training stimuli and then also the test questions. We will start by making the individual syllables that we will need.
- In particular, you will need to make 12 different syllables that will be combined to form larger words in the nonsense language. They are: 'ba', 'bu', 'bi', 'pa', 'pu', 'pi', 'ta', 'tu', 'ti', 'da', 'du', and 'di'.
- We will use the free IBM Watson Text-to-Speech [page](https://text-to-speech-demo.mybluemix.net/), select 'American English Allison' as your voice, and go to the 'Expressive SSML' tab.
- Click "Speak" to make sure you can hear the sample. I was unable to get this to work on Safari, but Chrome works fine.
- Now we need to get the synthesizer to produce the sounds we need. There are a few issues. First, we want the training to be in monotone with every syllable the same length. This requires some work. We need the speech to be fast enough, a relatively uniform speed, and a uniform pitch. Also, we need Watson to pronounce the syllables the way we want. I found that the following accomplishes that:

```
<speak><voice-transformation type="Custom" pitch="low" pitch_range="x-narrow" rate="x-fast">
la la la. bah. bee. boo. dah. dee. doo. pah. pee. poo. tah. tee. too. la la la  
</voice-transformation></speak>
```

- The reason for the 'lalala' at the beginning and the end is that I found that Watson often slowed down at the beginning or end of the text. So this provides a buffer. Notice that I ended up not needing to use IPA after all (it took a lot of tries to get this). But I expect we will need IPA for some of the follow-up expeirments.
- I downloaded this file. Then loaded it into [Audacity](http://www.audacityteam.org/download/) (an excellent open-source audio editor) to edit it. The result looks something like this:
![screenshot](figures/audacitydemo.png)
- The text of the paper says we want to produce stimuli at a rate of 216 syllables/minute. That means 277 ms per syllable. So I selected a 0.277 sec segment around the first syllable ("bah"), making sure I included all of it. Then I went to File -> Export Selected Audio in order to make a new wav file called "ba.wav".
- I repeated this process for each syllable. 
- Just to be safe, I re-opened every one of the syllable files to make sure they were 0.277 ms.
- In the original instructions, you were told to convert these to MP3 format right away. It turns out that doing so inserts an extra 50 ms of silence at the beginning of the file, messing up the timing. I had no idea that this was a thing, and I didn't penalize anyone who didn't notice (though one team did notice and managed to work around it!). Instead, I kept everything in WAV format until the last step.
- I then put my 12 wav syllable files into the 'stims' folder. 

## Make the Training Stimuli ##
- We are going to need a few things. I installed Homebrew (a package manager) and Sox (a shell audio editor) by opening up a shell and typing the following commands:

```
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" more stuff goes here
$ brew install lame
$ brew install sox --with-lame
```
- Now, we need to actually make the training sequences. This is complicated, since we need each of the 'words' to appear a certain number of times without any word ever repeating back-to-back. Jon and Sven have helpfully made a script that will do this for you.
- (BTW The text is actually inconsistent on how many times each word appears in the training. It says 300. However, if you were to have 21 minutes of training at 216 syllables/minute, you'd only get to 254 times. We went with the latter number.) 
- To use create the training stimuli, here is the command: 

```
python create_training.py
```

- After a minute, I had three files:  'training\_1.wav', 'training\_2.wav', and 'training\_3.wav'. Each one is 6:59 long.

## Make Question Stimuli ##
### Option 1: By hand ###
- Next, I made the test items. The paper explains the rules for creating nonword and part-word stimuli. You can follow these rules and determine what those words should be by hand.
- First, we make all the words, nonwords, and part-words using sox:

```
$ sox ba.wav bu.wav pu.wav babupu.wav
```

- The above command combines ba.wav, bu.wav, and pu.wav in order and saves the result as babupu.wav.
- Once we've made the 18 files (6 words, 6 nonwords, 6 part-words), we then need to make 36 test trials involving nonwords and 36 involving part-words.
- For both the nonword tests and the part-word tests, we needed to counter-balance whether the target word comes first for 18 of the stimuli and the foil comes first for the other 18. I did this so that for each 'real' word, it came first 3 times and last 3 times. 
- To actually make these, we'll also need to separate the word and foil by some silence. The easiest way to do this is to create a silence.wav using Audacity. Start a new file. Go to generate -> silence and generate 500 ms of silence (or some other reasonable length). Then export (File -> Export Audio) as a wav file and save as silence.wav.
- Now, you can make the test trials one at a time:

```
$ sox babupu.wav silence.wav pidata.wav babupu_pidata.wav
```
- Next, convert the training and trial files into mp3 format, either using a website (I linked to one in the guide) or Audacity, if you can get it to work (Audacity has some compatibility issues with macOS Sierra, and it can take some hacking to get mp3 output to work). This still adds the extra 50 ms to the beginning of each file, but that's no longer as big a deal (as opposed to adding 50 ms to ba.wav, bu.wav, etc., which adds up quickly!). 
- Finally, we put the resulting MP3s is static/audio.

### Option 2: Scripting ###
- The above steps should work just fine, but they take a while. Also, when you are doing things by hand, there are lots of opportunities for errors. And you don't have any record of what you've done that you can double-check. So I prefer to using programming to do as much as possible.
- Below, I'll show you how I created my sound files (you can also find the complete code in this repo as MakeFoils.R.). This requires some commands you don't know. I've added some comments both around the code and in it (comments follows the # symbol). I'm happy to answer any questions you have. 
- First, I figure out which syllables follow which syllables in the training:

```
words<-data.frame(word=c("babupu","bupada","dutaba","patubi","pidabu","tutibu"),s1="",s2="",s3="")
words$s1<-substr(words$word,1,2) #take the first two letters of each word in words$word
words$s2<-substr(words$word,3,4) #take the second two letters of each word in words$word
words$s3<-substr(words$word,5,6)

illegal<-data.frame(syllable=unique(c(words$s1,words$s2,words$s3)))
illegal[,unique(c(words$s1,words$s2,words$s3))]<-0
for (i in illegal$syllable){
	if (i %in% words$s1){ # x %in y = is x a member of list y?
		illegal[illegal$syllable==i,words$s2[words$s1==i]]<-1
		}
	if (i %in% words$s2){
		illegal[illegal$syllable==i,words$s3[words$s2==i]]<-1
		}
	if (i %in% words$s3){
		illegal[illegal$syllable==i,words$s1]<-1 #note that this only works because no words start and end with the same syllable
		}
}
```

- I now have a data frame that says which syllables could potentially follow each other in the training (note that we had to consider not only what syllables come after one another inside a word but also which ones might follow each other across words). Each row presents a syllable and the syllables that follow it (marked with 1) or do not (marked with 0). 

```
> illegal
   syllable ba bu du pa pi tu ta da ti pu bi
1        ba  1  1  1  1  1  1  0  0  0  0  0
2        bu  1  1  1  1  1  1  0  0  0  1  0
3        du  0  0  0  0  0  0  1  0  0  0  0
4        pa  0  0  0  0  0  1  0  1  0  0  0
5        pi  0  0  0  0  0  0  0  1  0  0  0
6        tu  0  0  0  0  0  0  0  0  1  0  1
7        ta  1  0  0  0  0  0  0  0  0  0  0
8        da  1  1  1  1  1  1  0  0  0  0  0
9        ti  0  1  0  0  0  0  0  0  0  0  0
10       pu  1  1  1  1  1  1  0  0  0  0  0
11       bi  1  1  1  1  1  1  0  0  0  0  0

```

- Nonwords should consist of three syllables that never follow each other in the training. Some students cleverly noticed that the syllable 'di' never appears in the training at all, so can follow and precede any other syllable. But that also would make it easy to detect the foils (nonwords and part-words), so I didn't use 'di' (hopefully the original authors didn't either!):

```
nonwords<-c()
forsamples<-data.frame(t(illegal[,2:12])) #t() transposes rows and columns.
colnames(forsamples)<-illegal$syllable
while (length(nonwords)<6){ #length(nonwords) = the length of our list 'nonwords'
	s1<-as.character(sample(illegal$syllable,1)) #sample(x,1) = choose 1 member of x at random
	s2<-as.character(sample(rownames(forsamples[forsamples[,s1]==0,]),1))
	s3<-as.character(sample(rownames(forsamples[forsamples[,s2]==0,]),1))
	nonwords<-c(nonwords,paste(s1,s2,s3,sep="")) #pastes s1,s2, & s3 together with nothing separating them
}
```

- Then I also created the part-words. I made sure to use the two that were explicitly mentioned in the text.

```
partwords<-c()
colnames(forsamples)<-illegal$syllable
#two part-words are mentioned. We add them at the end. Make the other four.
#first, make the ones that switch the final syllable
#we assume that transitional probability to final syllable should be 0 (text doesn't say)
while (length(partwords)<2){
	word<-as.character(sample(words$word[words$word!="pidabu"],1)) #if you don't use 'as.character', 'sample' returns a factor, and things get screwy.
	s1<-as.character(words$s1[words$word==word])
	s2<-as.character(words$s2[words$word==word])
	s3<-as.character(sample(rownames(forsamples[forsamples[,s2]==0,]),1))
	partwords<-c(partwords,paste(s1,s2,s3,sep=""))
}
#now, make the ones that switch the first sylalble
#we assume that transitional probability from first to second syllable should be 0 (text doesn't say)
while (length(partwords)<4){
	word<-as.character(sample(words$word[words$word!="dutaba"],1))
	s2<-as.character(words$s2[words$word==word])
	s3<-as.character(words$s3[words$word==word])
	s1<-as.character(sample(rownames(forsamples[forsamples[,s1]==0,]),1))
	partwords<-c(partwords,paste(s1,s2,s3,sep=""))
}
partwords<-c(partwords,"pidata","bitaba") #these two part-words are actually mentioned in the text
```

- Now we need to actually make the words. In Option 1, we typed in the commands one at a time into the Terminal. But we can use R to write our commands for us. Again, remember that substr(s,x,y) is used to pull out all the letters from the string 's' starting with the xth letter and ending with the yth letter. Also remember that paste(a,b,...,sep=x) will combine strings a, b, ..., together, separated by the string x (usually, we make x="" to have no separation).

```
#first to create the words
sox<-""
for (w in words$word){
	w<-as.character(w)
	s1<-substr(w,1,2)
	s2<-substr(w,3,4)
	s3<-substr(w,5,6)
	sox<-paste(sox,(paste("sox ",s1,".wav ",s2,".wav ",s3,".wav ",w,".wav;",sep="")))
}

#nonwords
for (w in nonwords){
	w<-as.character(w)
	s1<-substr(w,1,2)
	s2<-substr(w,3,4)
	s3<-substr(w,5,6)
	sox<-paste(sox,(paste("sox ",s1,".wav ",s2,".wav ",s3,".wav ",w,".wav;",sep="")))
}

#part-words
for (w in partwords){
	w<-as.character(w)
	s1<-substr(w,1,2)
	s2<-substr(w,3,4)
	s3<-substr(w,5,6)
	sox<-paste(sox,(paste("sox ",s1,".wav ",s2,".wav ",s3,".wav ",w,".wav;",sep="")))
}

#now the questions in the non-words condition
for (w in words$word){
	nws<-sample(nonwords,length(nonwords)) #randomize the order of the nonwords. Reason will be clear shortly.
	for (nw in nws[1:(length(nws)/2)]){
		#first half of the nonwords. Have the foil come second.
		sox<-paste(sox,(paste("sox ",w,".wav ","silence.wav ",nw,".wav ","n2_",w,"_",nw,".wav;",sep="")))
	}
	for (nw in nws[(length(nws)/2+1):length(nws)]){
		#first half of the nonwords. Have the foil come second.
		sox<-paste(sox,(paste("sox ",nw,".wav ","silence.wav ",w,".wav ","n1_",nw,"_",w,".wav;",sep="")))
	}
}

#now same for the part-words
for (w in words$word){
	pws<-sample(partwords,length(partwords)) #randomize the order of the nonwords. Reason will be clear shortly.
	for (pw in pws[1:(length(pws)/2)]){
		#first half of the nonwords. Have the foil come second.
		sox<-paste(sox,(paste("sox ",w,".wav ","silence.wav ",pw,".wav ","p2_",w,"_",pw,".wav;",sep="")))
	}
	for (pw in pws[(length(pws)/2+1):length(pws)]){
		#first half of the nonwords. Have the foil come second.
		sox<-paste(sox,(paste("sox ",pw,".wav ","silence.wav ",w,".wav ","p1_",pw,"_",w,".wav;",sep="")))
	}
}
```

- Now, I have a list of all the sox commands I need, separated by semicolons (which in Terminal is the same as pressing 'Enter'):

![screenshot](figures/soxcommands.png)

- This can be copied and pasted into Terminal and then run. It takes a while but makes all our files at once.
- Now we need to convert from wav format to mp3. Again, this is annoying to do one file at a time. With a little sox-fu, though, we can do it all at once:

```
$ for f in *.wav; do sox "$f" -t mp3 new/"${f/.wav/.mp3}"; done
```

- Note again that the semicolon is the same as pressing 'Enter'. So you could have done:

```
$ for f in *.wav
> do sox "$f" -t mp3 "${f/.wav/.mp3}"
> done
```

- This is complicated. Remember from the Terminal tutorial that the asterisk is a wildcard and stands for anything, so *.wav is a list of every file in the folder ending with ".wav". So we start by telling Terminal that we want to go through every wav folder in our file one at a time. We will call the current file in the list 'f'. 
- Now, we want to 'do' something. Specifically, we want to run sox on file f. We want to turn it into a mp3 (that's what the "-t mp3" means). The last bit of the line (new/...) tells us the location and name of the output file. The middle part is `{f/.wav/.mp3}`, which means "take the value of `f` and replace ".wav" with ".mp3". So if `f` was "ba.wav", change that to "ba.mp3". 
- The final line (`done	`) tells the Terminal that we are done with the current file and to go on to the next one. 
- This takes a while to run. Once it is done, I moved the mp3 files that I needed to static/audio.
- If all the above is bewildering and complex, you have three options. First, you can talk with about about it and try to understand it. Or, you could not both understanding it but know that these are the kinds of things you can do if you learn a bit more programming. Or you can just ignore it entirely and be happy doing things (more slowly) by hand. For the purposes of this class, any of those options are fine.

## Creating the Psiturk Experiment Using the Stimuli You Created ##
- We are going to use [psiTurk](https://psiturk.org/) to run the experiment. I recommend reading about PsiTurk using the link in the previous sentence. 
- The one thing that Jon couldn't program in was the list of stimuli, since he doesn't know what they are! We'll edit a file in static/js called "stims.js". You can use any editor, but Text Edit or Word may try to do things to the file that you don't want. So I think it's easier to edit it using the terminal program pico:
```
$ cd ../static/js
$ pico stims.js
```
- Note that your mouse probably won't work, and that you'll need to navigate using the arrow keys. Instead of the menubar (which is for Terminal, not pico!), you see a list of commands at the bottom of your terminal window: "^G Get Help", "^X Exit" and so on. The carrot (^) stands for the 'control' key. So to exit, you want control-X. You will be prompted to save. When the time comes, make sure you save your changes!
- It should look like this:
- 
```
/* remember to make sure that all the necessary audio files are in the '/static/audio' folder! */

var stims = {

        /* will only present the first 3 */
        training  : [
                '1.mp3',
                '2.mp3',
                '3.mp3'
        ],

        /* will present however many are added here */
        practice  : [
                '1.mp3',
                '2.mp3',
                '3.mp3'
        ],

        /* will present however many are added here */
        questions : [
                '1.mp3',
                '2.mp3',
                '3.mp3'
        ]

};
static/js/stims.js (END)
```

- I think updated the lists of stimuli to match my actual stimulus files. Since I don't have any practice trials for this experiment, I simply made that one blank (practice : [ ],).
- You can see what the finished stims.js file looked like in this repo.

## Taking the Experiment for a Test Drive ##
- Once again, we had to install some stuff. The first one is a utility that we'll use to save the results of the experiment. You don't need to know what this does, but you do need it to run psiTurk.

```
$ brew install mysql-connector-c
```

- Macs automatically have python installed. For some people, the built-in version worked fine. For others, it did not. You can't modify the installed version (which you'd never actually want to use -- I don't know why Mac has it there to begin with), so we do an end run around it. First, we install an updated version of python in a separate location:

```
$ brew install python
```

- Now we need to make sure your computer automatically uses that version of python, not the ancient built-in one. Don't worry, if for some reason you ever wanted to use the built-in one, it's still there and you'd be able to. The following will work on many -- but unfortunately not all! -- Macs. 

```
$ Echo "export PATH=\"/usr/local/bin:/usr/local/sbin:$PATH\"" | tee -a ~/.bash_profile
```

- To test whether this worked, first close the Terminal. Then open it again and type `python --version`. It should tell you that your version of Python is 2.7.13 or later:

```
$ python --version
Python 2.7.13
```

- If it lists an earlier verison of Python, you are the lucky winner and will need some help. I have not yet figured out why this works for some but not others.
- At this point, you should have pip installed. You can check as follows:

```
$ pip -- version
pip 9.0.1 from /usr/local/lib/python2.7/site-packages (python 2.7)
```

- If you see something like the above, you are good. If you see "command not found", you are again the lucky winner. Talk to an instructor. 
- Now, I installed virtualenvwrapper:

```
$ sudo -H pip install virtualenvwrapper --ignore-installed six
```

- That last command will prompted my for my computer password, which I entered. I did not get any error messages (again, error messages are bad and require talking to an instructor). 
- A virtual python environment is basically a special copy of Python and its libraries. So you can make a virtual environment for one program (say psiTurk) that has exactly the versions of the libraries that program needs. And you can make a different virtual environment for other software. So they can live happily side-by-side on your computer. Notice how this is similar (but different) from the notion of a repository fork.
- I then confirmed that the file virtualenvwrapper.sh has been installed on your computer:

```
$ ls /usr/local/bin/virtualenvwrapper.sh
/usr/local/bin/virtualenvwrapper.sh
```

- Only once my output looked like what is shown above did I do the following:

```
$ export WORKON_HOME=~/Envs
$ mkdir -p $WORKON_HOME
$ Echo "export WORKON_HOME=\"~/Envs\"" | tee -a ~/.bash_profile
$ Echo "source /usr/local/bin/virtualenvwrapper.sh" | tee -a ~/.bash_profile
$ Echo "deactivateonexit () { deactivate;}" | tee -a ~/.bash_profile
$ Echo "trap deactivateonexit EXIT" | tee -a ~/.bash_profile
```

- I did not get any error messages (again, error messages mean that you should stop and fix whatever is wrong and not continue to the next step). I think closed and reopened the Terminal. THen I set up a virtual environment for psiTurk:

```
$ mkvirtualenv psiturk
```

- Thist created a virtual environment called 'psiturk'. I could have called it 'Ishmael', but 'psiturk' is easier to remember since we'll use this virtual environment for psiTurk:

```
$ workon psiturk
(psiturk) $ pip install psiturk
(psiturk) $ pip install mysql-python
```

- psiTurk installed without error. So I navigated to the root folder of the repo and launched the experiment locally:

```
(psiturk) $ psiturk
(psiturk) $ server on
(psiturk) $ debug
```

- That launched the experiment in a new browser window. And it all ran correctly!